'use strict';
import { APIGatewayEvent } from 'aws-lambda';
import { Response } from '../../libs/response';
import { DynamoDBLicenseRepository } from '../../repository/DynamoDBLicenseRepository';

const getLicense = async (): Promise<any> => {
  let data;
  const dbClient = new DynamoDBLicenseRepository();
  try {
    data = await dbClient.findAll();
  } catch (error) {
    data = error;
  }
  const response = new Response(200, data);
  return response;
}

const getLicenseByCode = async (event: APIGatewayEvent): Promise<any> => {
  console.log(event.pathParameters);
  if (event.pathParameters === null) {
    return null;
  }
  const email = event.pathParameters.email;
  const license = event.pathParameters.license;
  let data;
  try {
    const dbClient = new DynamoDBLicenseRepository();
    data = await dbClient.findByLicenseEmail(email, license);
  } catch (error) {
    data = error;
  }
  return new Response(200, data);
}

module.exports = {
  getLicense,
  getLicenseByCode
}