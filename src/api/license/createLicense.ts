'use strict';
import { APIGatewayEvent } from 'aws-lambda';
import { Response } from '../../libs/response';
import { DynamoDBLicenseRepository } from '../../repository/DynamoDBLicenseRepository';
import License from '../../domain/license';

const createLicense = async (event: APIGatewayEvent): Promise<any> => {
    if (event.body === null) {
        return null;
    }
    const body = event.body;
    const params = JSON.parse(body);
    console.log(params);
    const license: License = new License(
        params.code,
        params.email,
        params.first_name,
        params.last_name,
        params.license_key
    )
    let data;
    try {
      const dbClient = new DynamoDBLicenseRepository();
      dbClient.createLicense(license);
    } catch (error) {
      data = error;
    }
    return new Response(200, data);
  }
  
  module.exports = {
    createLicense
  }