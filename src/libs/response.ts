class Response {
    headers: Object;
    statusCode: Number;
    body: string;
    isBase64Encoded: boolean;
    constructor(code: Number, data: Object) {
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        };
        this.statusCode = code;
        this.body = JSON.stringify(
            {
                data
            },
            null,
            2
        ),
        this.isBase64Encoded = false;
    }
}

export { Response };