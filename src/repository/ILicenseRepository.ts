import License from "../domain/license";

interface ILicenseRepository {
    findAll(): Promise<any>;
    findByLicenseEmail(email: string, license: string): Promise<any>;
    createLicense(license: License): Promise<any>;
}

export { ILicenseRepository }