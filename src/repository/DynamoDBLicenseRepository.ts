import { ILicenseRepository } from './ILicenseRepository';
import * as AWS from 'aws-sdk';
import { ScanInput, QueryInput, PutItemInput } from 'aws-sdk/clients/dynamodb';
import License from '../domain/license';

class DynamoDBLicenseRepository implements ILicenseRepository {

    dynamoDB: AWS.DynamoDB;
    dynamoDBDocumentClient: AWS.DynamoDB.DocumentClient;

    constructor() {
        this.dynamoDB = new AWS.DynamoDB();
        this.dynamoDBDocumentClient = new AWS.DynamoDB.DocumentClient();
    }

    async findAll(): Promise<any> {
        let data;
        const params: ScanInput = {
            TableName: process.env.TABLE_NAME!
        };
        try {
          data = await this.dynamoDBDocumentClient.scan(params).promise();
        } catch (error) {
          data = error;
        }
        return data;
    }

    async findByLicenseEmail (email: string, license: string): Promise<any> {
      const queryParams: QueryInput = {
          TableName: process.env.TABLE_NAME!,
          KeyConditionExpression: '#code = :code and #email = :email',
          ExpressionAttributeNames: {
            "#code": "code",
            "#email": "email"
          },
          ExpressionAttributeValues: {
              ':code' : { 'S': license },
              ':email': { 'S': email}
          }
      };
      let data;
      try {
        data = await this.dynamoDB.query(queryParams).promise();
      } catch (error) {
        data = error;
      }
      return data;
    }

    async createLicense (license: License) {
      const queryParams = {
        TableName: process.env.TABLE_NAME!,
        Item: {
          "code": license.code,
          "email": license.email,
          "first_name": license.first_name,
          "last_name": license.last_name,
          "license_key": license.license_key
        }
      };
      let data;
      try {
        data = await this.dynamoDBDocumentClient.put(queryParams).promise();
      } catch (error) {
        data = error;
      }
      return data;
    }
}

export { DynamoDBLicenseRepository }

