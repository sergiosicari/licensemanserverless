import { threadId } from "worker_threads";

class License {
    code: string;
    email: string;
    first_name: string;
    last_name: string;
    license_key: string;
    constructor (
        code: string, 
        email: string, 
        first_name: string, 
        last_name: string, 
        license_key: string
    ) {
        this.code = code;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.license_key = license_key;
    }
}

export default License;